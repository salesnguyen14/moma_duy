package com.amplio.moma_duy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;


public class ModernArtUI extends ActionBarActivity {

    private final static String MOMA_URL ="http://www.moma.org";
    private final static int TL_COLOR = Color.RED;
    private final static int BL_COLOR = Color.BLUE;
    private final static int TR_COLOR = Color.GREEN;
    private final static int MR_COLOR = Color.GRAY;
    private final static int BR_COLOR = Color.MAGENTA;

    private LinearLayout topLeft;
    private LinearLayout bottomLeft;
    private LinearLayout topRight;
    private LinearLayout middleRight;
    private LinearLayout bottomRight;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modern_art_ui);

        setDefaults();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeColor(topLeft, TL_COLOR, progress);
                changeColor(bottomLeft, BL_COLOR, progress);
                changeColor(topRight, TR_COLOR, progress);
                changeColor(bottomRight, BR_COLOR, progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modern_art_ui, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.more_info) {
            openPopupDialog();
        }
        return true;
        //return super.onOptionsItemSelected(item);
    }

    private void setDefaults(){
        seekBar = (SeekBar) findViewById(R.id.seekbar);

        topLeft = (LinearLayout) findViewById(R.id.topLeft);
        topLeft.setBackgroundColor(TL_COLOR);

        bottomLeft = (LinearLayout) findViewById(R.id.bottomLeft);
        bottomLeft.setBackgroundColor(BL_COLOR);

        topRight = (LinearLayout) findViewById(R.id.topRight);
        topRight.setBackgroundColor(TR_COLOR);

        middleRight = (LinearLayout) findViewById(R.id.middleRight);
        middleRight.setBackgroundColor(MR_COLOR);

        bottomRight = (LinearLayout) findViewById(R.id.bottomRight);
        bottomRight.setBackgroundColor(BR_COLOR);
    }

    private void changeColor(LinearLayout linearLayout, int InitialColor, int progress){
        float[] hsv = new float[3];
        Color.colorToHSV(InitialColor, hsv);
        hsv[0] += progress;
        linearLayout.setBackgroundColor(Color.HSVToColor(hsv));
    }

    private void openPopupDialog() {
         new AlertDialog.Builder(this)
                 .setTitle("Visit MOMA")
                 .setMessage("Are you sure?")
                 .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         dialog.cancel();
                     }
                 })
                 .setNegativeButton("YES", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         Intent openWebsite = new Intent(Intent.ACTION_VIEW, Uri.parse(MOMA_URL));
                         startActivity(openWebsite);
                     }
                 })
                .show();
    }
}
